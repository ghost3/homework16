#include <iostream>
#include <iomanip>

int main()
{
	const int N = 10;

	int arr[N][N];

	//filling the array
	for (int i = 0; i < N; ++i)
		for (int j = 0; j < N; ++j)
			arr[i][j] = i + j;

	//printing the array
	for (int i = 0; i < N; ++i) 
	{
		std::cout << '\n';
		for (int j = 0; j < N; ++j)
			std::cout << std::setw(2) << arr[i][j] << ' ';
	}

	//printing sum of the specified line
	int today = 0;	//date to input
	int sum = 0;	//sum of all digits in line

	std::cout << "\n\nWhat date is today?\n";
	std::cin >> today;

	std::cout << "\nYour line today is:\n";
	for (int i = 0; i < N; ++i)
	{
		std::cout << arr[today % N][i] << ' ';	//prints the line that you have "selected"
		sum += arr[today % N][i];				//summing aall digits in this line
	}

	std::cout << "\nSum of all digits in your line:\n";
	std::cout << sum << std::endl;
}
